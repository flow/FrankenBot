yml configuration file
======================

```yml
accounts:
 - jid: juliet@example.org
   pass: iloveyou
   certpin: <certpin>
   masters:
     - romeo@examle.org
   mucs:
   	 - balcony@muc.example.org
	 - chamber@muc.example.org
```
