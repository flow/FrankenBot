/**
 *
 * Copyright 2017 Florian Schmaus
 *
 * This file is part of FrankenBot.
 *
 * FrankenBot is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 */
package eu.geekplace.frankenbot.core.configuration

import org.scalatest._
import org.jxmpp.jid.impl.JidCreate

class FrankenBotConfigurationTest extends FunSuite with Matchers {

  test("YAML Test") {
    val stream = getClass().getResourceAsStream("/FrankenBot.example.yml")
    val frankenBotConfiguration = FrankenBotYamlConfigurationUtil.parseConfig(stream)

    val general = frankenBotConfiguration.general
    general.name should be ("Hilde")

    val accounts = frankenBotConfiguration.xmpp.accounts
    accounts.size should be (1)
    val account = accounts.head
    account.jid should be (JidCreate.from("juliet@example.org"))
    account.pass should be ("iloveyou")
    account.certpin should be (Some("<certpin>"))

    account.mucs.size should be(2)
    account.mucs(0) should be(JidCreate.from("chamber@muc.example.org"))
    account.mucs(1) should be(JidCreate.from("balcony@muc.example.org"))

    val masters = frankenBotConfiguration.xmpp.masters
    masters.size should be(1)
    masters.head should be(JidCreate.from("romeo@example.org"))
    masters.head should be(JidCreate.from("romeo@example.org"))

    val monitoredJids = frankenBotConfiguration.maxsMonitor.monitoredJids
    monitoredJids.size should be(3)
  }
}