/**
 *
 * Copyright 2017 Florian Schmaus
 *
 * This file is part of FrankenBot.
 *
 * FrankenBot is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 */
package eu.geekplace.frankenbot.plugin.maxsmonitor

import scala.concurrent.duration._

import org.jxmpp.jid.EntityBareJid

import akka.actor.Actor
import akka.pattern.ask
import akka.util.Timeout
import eu.geekplace.frankenbot.core.xmpp._
import akka.actor.ActorRef
import org.projectmaxs.xmpp.MaxsStatusExtensionElement
import akka.actor.Timers
import eu.geekplace.frankenbot.core.xmpp.XmppPresenceActor.SinglePresence
import org.rrd4j.graph.RrdGraphDef
import org.rrd4j.ConsolFun
import org.rrd4j.graph.RrdGraph
import akka.actor.ActorLogging
import java.awt.Color
import java.nio.file.Path
import java.nio.file.Paths

object MaxsMonitorStatusActor {
    case class CheckStatus(jid: EntityBareJid)
    case class NewCheckStatusPeriod()
}

class MaxsMonitorStatusActor(maxsMonitor: MaxsMonitor, xmppPresenceActor: ActorRef) extends Actor
                                                                                    with Timers
                                                                                    with ActorLogging {
  import MaxsMonitorStatusActor._
  implicit val timeout = Timeout(5.seconds)

  timers.startPeriodicTimer(this, NewCheckStatusPeriod, 5.minute)

  def receive = {
    case NewCheckStatusPeriod => {
        for (maxsMonitorRrd <- maxsMonitor.jidToRrdb.values) {
          val httpDirectory = maxsMonitor.frankenBot.config.httpDirectory
          drawBatteryPercentageGraph(maxsMonitorRrd, httpDirectory)
          drawBatteryTemperatureGraph(maxsMonitorRrd, httpDirectory)
          drawBatteryVoltageGraph(maxsMonitorRrd, httpDirectory)
        }
        for (jid <- maxsMonitor.configuration.monitoredJids) {
          self ! CheckStatus(jid)
        }
    }
    case CheckStatus(jid) => {
      log.debug(s"Requesting status update for $jid")
      xmppPresenceActor ! XmppPresenceActor.GetSingleAvailablePresence(jid)
    }
    case SinglePresence(presence) => {
      log.debug(s"Received single presence $presence")
      case class MaxsStatus(maxsStatusExtensionElement: MaxsStatusExtensionElement, from: EntityBareJid)
      for (maxsStatus <- for {
          maxsStatusExtensionElement <- Option(MaxsStatusExtensionElement from presence)
          from <- Option(presence getFrom() asEntityBareJidIfPossible())
        } yield MaxsStatus(maxsStatusExtensionElement, from)) {
        val db = maxsMonitor.jidToRrdb(maxsStatus.from).rrddb
        val batteryPercentage =  maxsStatus.maxsStatusExtensionElement.getBatteryPercentage
        val batteryTemperature = maxsStatus.maxsStatusExtensionElement.getBatteryTemperature
        val batteryVoltage = maxsStatus.maxsStatusExtensionElement.getBatteryVoltage
        db.createSample()
          .setValue("battery-percentage", batteryPercentage)
          .setValue("battery-temperature", batteryTemperature)
          .setValue("battery-voltage", batteryVoltage)
          .update()
        log.info(s"Inserted values for ${maxsStatus.from}: battery-percentage=$batteryPercentage battery-temperature=$batteryTemperature battery-voltage=$batteryVoltage")
      }
    }
  }

  private def drawBatteryPercentageGraph(maxsMonitorRrd: MaxsMonitorRrd, httpDirectory: Path) = {
    val dsName = "battery-percentage"

    val graphFile = Paths.get(httpDirectory.toString, maxsMonitorRrd.jid + "-" + dsName + "-24h.png")

    val graphDef = new RrdGraphDef();
    graphDef setWidth 1000
    graphDef setHeight 600
    graphDef setFilename graphFile.toString
    graphDef setImageFormat "png"
    graphDef setTitle ("Battery - " + maxsMonitorRrd.jid)
    graphDef setVerticalLabel "Percentage"
    graphDef datasource (dsName + "-average", maxsMonitorRrd.rrddb.getPath, dsName, ConsolFun.AVERAGE)
    graphDef datasource (dsName + "-min", maxsMonitorRrd.rrddb.getPath, dsName, ConsolFun.MIN)
    graphDef datasource (dsName + "-max", maxsMonitorRrd.rrddb.getPath, dsName, ConsolFun.MAX)

    graphDef line (dsName + "-average", Color.BLUE, "Average Battery")
    graphDef line (dsName + "-min", Color.RED, "Mininum Battery")
    graphDef line (dsName + "-max", Color.GREEN, "Maximum Battery")

    val graph = new RrdGraph(graphDef)
    val graphInfo = graph.getRrdGraphInfo
    log.info("Created RRD Graph " + graphInfo.getFilename + " (" + graphInfo.getByteCount + " bytes)")
  }

  private def drawBatteryTemperatureGraph(maxsMonitorRrd: MaxsMonitorRrd, httpDirectory: Path) = {
    val dsName = "battery-temperature"

    val graphFile = Paths.get(httpDirectory.toString, maxsMonitorRrd.jid + "-" + dsName + "-24h.png")

    val graphDef = new RrdGraphDef();
    graphDef setWidth 1000
    graphDef setHeight 600
    graphDef setFilename graphFile.toString
    graphDef setImageFormat "png"
    graphDef setTitle ("Battery Temperature - " + maxsMonitorRrd.jid)
    graphDef setVerticalLabel "Celsius"
    graphDef datasource (dsName + "-average", maxsMonitorRrd.rrddb.getPath, dsName, ConsolFun.AVERAGE)
    graphDef datasource (dsName + "-min", maxsMonitorRrd.rrddb.getPath, dsName, ConsolFun.MIN)
    graphDef datasource (dsName + "-max", maxsMonitorRrd.rrddb.getPath, dsName, ConsolFun.MAX)

    graphDef line (dsName + "-average", Color.BLUE, "Average Temperature")
    graphDef line (dsName + "-min", Color.RED, "Mininum Temperature")
    graphDef line (dsName + "-max", Color.GREEN, "Maximum Temperature")

    val graph = new RrdGraph(graphDef)
    val graphInfo = graph.getRrdGraphInfo
    log.info(s"Created RRD Graph ${graphInfo.getFilename} ( ${graphInfo.getByteCount} bytes )")
  }

  private def drawBatteryVoltageGraph(maxsMonitorRrd: MaxsMonitorRrd, httpDirectory: Path) = {
    val dsName = "battery-voltage"

    val graphFile = Paths.get(httpDirectory.toString, maxsMonitorRrd.jid + "-" + dsName + "-24h.png")
    val graphDef = new RrdGraphDef();
    graphDef setWidth 1000
    graphDef setHeight 600
    graphDef setFilename graphFile.toString
    graphDef setImageFormat "png"
    graphDef setTitle ("Battery Voltage - " + maxsMonitorRrd.jid)
    graphDef setVerticalLabel "mV"
    graphDef datasource (dsName + "-average", maxsMonitorRrd.rrddb.getPath, dsName, ConsolFun.AVERAGE)
    graphDef datasource (dsName + "-min", maxsMonitorRrd.rrddb.getPath, dsName, ConsolFun.MIN)
    graphDef datasource (dsName + "-max", maxsMonitorRrd.rrddb.getPath, dsName, ConsolFun.MAX)

    graphDef line (dsName + "-average", Color.BLUE, "Average Voltage")
    graphDef line (dsName + "-min", Color.RED, "Mininum Voltage")
    graphDef line (dsName + "-max", Color.GREEN, "Maximum Voltage")

    val graph = new RrdGraph(graphDef)
    val graphInfo = graph.getRrdGraphInfo
    log.info("Created RRD Graph " + graphInfo.getFilename + " (" + graphInfo.getByteCount + " bytes)")
  }

}