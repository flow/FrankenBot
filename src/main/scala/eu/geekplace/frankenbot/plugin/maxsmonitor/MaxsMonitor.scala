/**
 *
 * Copyright 2017 Florian Schmaus
 *
 * This file is part of FrankenBot.
 *
 * FrankenBot is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 */
package eu.geekplace.frankenbot.plugin.maxsmonitor

import org.jivesoftware.smack.XMPPConnection
import org.jivesoftware.smack.roster.Roster
import org.jivesoftware.smack.AbstractConnectionListener
import org.jivesoftware.smack.roster.RosterUtil
import org.jivesoftware.smack.roster.AbstractPresenceEventListener
import org.jxmpp.jid.FullJid
import org.jivesoftware.smack.packet.Presence
import org.jxmpp.jid.EntityBareJid
import org.rrd4j.core.RrdDef
import org.rrd4j.DsType
import org.rrd4j.ConsolFun
import org.rrd4j.core.RrdDb
import java.nio.file.Paths
import scala.collection.mutable.HashMap
import eu.geekplace.frankenbot.core.FrankenBot
import org.projectmaxs.xmpp.MaxsStatusExtensionElementProvider
import eu.geekplace.frankenbot.core.configuration.MaxsMonitorConfiguration
import java.nio.file.Path
import akka.actor.Props
import com.typesafe.scalalogging.StrictLogging
import eu.geekplace.frankenbot.plugin.maxsmonitor.MaxsMonitorStatusActor.NewCheckStatusPeriod
import org.jivesoftware.smack.roster.SubscribeListener
import org.jivesoftware.smack.roster.SubscribeListener.SubscribeAnswer
import org.jxmpp.jid.Jid

object MaxsMonitor {
  MaxsStatusExtensionElementProvider.setup
}

case class MaxsMonitorRrd(jid: EntityBareJid, rrddb: RrdDb)

class MaxsMonitor(val frankenBot: FrankenBot) extends StrictLogging {

  val configuration = frankenBot.config.yamlConfiguration.maxsMonitor

  // Initialize the MaxsMonitor by running the MaxsMonitor object code.
  MaxsMonitor

  val maxsMonitorStatusActor = frankenBot.actorSystem.actorOf(
      Props(classOf[MaxsMonitorStatusActor], this, frankenBot.xmppPresenceActor),
      name = "MaxsMonitorStatusActor")

  // Create the RRD DBs
  val jidToRrdb = HashMap[EntityBareJid, MaxsMonitorRrd]()

  for (jid <- frankenBot.config.yamlConfiguration.maxsMonitor.monitoredJids) {
    val rrdb = getRrdForJid(jid)
    jidToRrdb(jid) = rrdb
  }

  for (fbc <- frankenBot.connections) {
    val connection = fbc.xmppconnection
    val roster = Roster getInstanceFor connection

    connection.addConnectionListener(new AbstractConnectionListener() {
      override def authenticated(connection: XMPPConnection, resumed: Boolean) {
        if (resumed) return

        // Ensure we are subscribed.
        frankenBot.config.yamlConfiguration.maxsMonitor.monitoredJids.foreach(
            RosterUtil askForSubscriptionIfRequired (roster, _)
        )
      };
    })

    roster addSubscribeListener (new SubscribeListener() {
      override def processSubscribe(from: Jid, subscribeRequest: Presence): SubscribeAnswer = {
        for (monitoredJid <- configuration.monitoredJids) {
          if (from equals monitoredJid) {
            // Check if we are also subscribed to monitored JID.
            // If not, retry the subscription attempt, since it obviously sees us now as a friend.
            RosterUtil.askForSubscriptionIfRequired(roster, monitoredJid)
            return SubscribeAnswer.Approve
          }
        }
        null
      }
    })

    // TODO: Remove this.
    roster addPresenceEventListener (new AbstractPresenceEventListener() {
      override def presenceAvailable(jid: FullJid, presence: Presence) {
        logger.info("Available presence from " + jid + ": " + presence)
      }

      override def presenceUnavailable(jid: FullJid, presence: Presence) {
        logger.info("Unavailable presence from " + jid + ": " + presence)
      }
    })
  }

  def frankenkBotInitializtionComplete() = {
    // Kickoff monitoring.
    maxsMonitorStatusActor ! NewCheckStatusPeriod
  }

  def frankenBotShutdown() = {
    for (rrdDb <- jidToRrdb.values.map(v => v.rrddb)) {
      logger info s"Closing RRDDB ${rrdDb.getPath}"
      rrdDb.close
    }
  }

  private def getRrdForJid(jid: EntityBareJid): MaxsMonitorRrd = {
    val rrdPath = Paths.get(frankenBot.config.dataDirectory.toString, jid + ".rrd")

    val baseStepIntervalSeconds = 300
    val rrdDef = new RrdDef(rrdPath.toString, baseStepIntervalSeconds)

    val heartbeatSeconds = baseStepIntervalSeconds + 120

    rrdDef addDatasource ("battery-percentage", DsType.GAUGE, heartbeatSeconds, 0, 1)
    rrdDef addDatasource ("battery-temperature",  DsType.GAUGE, heartbeatSeconds, -237.15, 1000)
    rrdDef addDatasource ("battery-voltage",  DsType.GAUGE, heartbeatSeconds, 0, 10000)

    val averageDays = 365
    val averageArchiveSteps = 1 // baseStepIntervalSeconds
    val averageArchiveRows = (averageDays * 24 * 60 * 60) / baseStepIntervalSeconds
    rrdDef addArchive (ConsolFun.AVERAGE, 0.5, averageArchiveSteps, averageArchiveRows)

    val minDays = 365
    val minTimespanHours = 3
    val minArchiveSteps = (minTimespanHours * 60 * 60) / baseStepIntervalSeconds
    val minArchiveRows = (minDays * 24 * 60 * 60) / baseStepIntervalSeconds
    rrdDef addArchive (ConsolFun.MIN, 0.5, minArchiveSteps, minArchiveRows)

    val maxDays = 365
    val maxTimespanHours = 3
    val maxArchiveSteps = (maxTimespanHours * 60 * 60) / baseStepIntervalSeconds
    val maxArchiveRows = (maxDays * 24 * 60 * 60) / baseStepIntervalSeconds
    rrdDef addArchive (ConsolFun.MAX, 0.5, maxArchiveSteps, maxArchiveRows)

    if (!rrdPath.toFile.isFile) {
      val rrdDb = new RrdDb(rrdDef)
      return MaxsMonitorRrd(jid, rrdDb)
    }

    var rrdDb = new RrdDb(rrdPath.toString())
    if (!rrdDb.getRrdDef().equals(rrdDef)) {
        logger.warn("RrdDb defintion out of date, recreating")
        rrdDb close()
        rrdPath.toFile.delete()
        rrdDb = new RrdDb(rrdDef)
    }

    return MaxsMonitorRrd(jid, rrdDb)
  }
}

