/**
 *
 * Copyright 2017 Florian Schmaus
 *
 * This file is part of FrankenBot.
 *
 * FrankenBot is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 */
package eu.geekplace.frankenbot.core

import scala.language.postfixOps
import scala.concurrent.duration._
import java.util.concurrent.CountDownLatch
import org.jivesoftware.smack.MessageListener
import org.jivesoftware.smack.StanzaListener
import org.jivesoftware.smack.filter.AndFilter
import org.jivesoftware.smack.filter.FromMatchesFilter
import org.jivesoftware.smack.filter.MessageWithBodiesFilter
import org.jivesoftware.smack.packet.Message
import org.jivesoftware.smack.packet.Stanza
import org.jivesoftware.smack.tcp.XMPPTCPConnection
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration
import org.jivesoftware.smackx.muc.MultiUserChatManager
import org.jxmpp.jid.impl.JidCreate
import org.jxmpp.jid.parts.Resourcepart
import eu.geekplace.javapinning.JavaPinning
import org.jivesoftware.smackx.muc.DiscussionHistory
import org.jivesoftware.smack.roster.Roster
import scala.collection.mutable.ArrayBuffer
import org.jivesoftware.smack.filter.OrFilter
import scala.collection.JavaConverters._
import eu.geekplace.javapinning.java7.Java7Pinning
import eu.geekplace.frankenbot.core.configuration.FrankenBotYamlConfiguration
import eu.geekplace.frankenbot.core.configuration.FrankenBotYamlConfigurationUtil
import eu.geekplace.frankenbot.core.configuration.XmppAccountConfiguration
import org.apache.commons.lang3.time.DurationFormatUtils
import com.typesafe.scalalogging.StrictLogging
import eu.geekplace.frankenbot.plugin.maxsmonitor.MaxsMonitor
import akka.actor.ActorSystem
import akka.actor.Props
import eu.geekplace.frankenbot.core.xmpp.XmppPresenceActor
import org.jivesoftware.smack.SmackConfiguration
import scala.concurrent.Await
import org.slf4j.bridge.SLF4JBridgeHandler

object FrankenBot {
  // Mostly static FrankenBot initialization
  SmackConfiguration.DEBUG = true

  SLF4JBridgeHandler install()

  val actorSystem = ActorSystem("FrankenBotActorSystem")

}

/**
 * Initialization phases:
 * - Actor System base initialization
 * - Setup connections, but do not connect
 * - FrankenBot Actors
 * - Load plugins, so that they can install hooks
 * - Connect the connections
 * 
 */
class FrankenBot(val config: FrankenBotConfiguration) extends StrictLogging {
  FrankenBot

  val actorSystem = FrankenBot.actorSystem

  // Initialize but do not start connections.
  val connections = initializeConnections

  // Start the actor(s) *after* the connections have been craeted. Because some actors need the default connectins etc.
  val xmppPresenceActor = actorSystem.actorOf(Props(classOf[XmppPresenceActor], this), name = "XmppPresenceActor")

  // Load the Plugins, so that they can itself install their hooks (but they should not "start" yet).
  val maxsMonitor = new MaxsMonitor(this)

  for (fbc <- connections) {
    logger.info("Connecting XMPP connection " + fbc.xmppconnection)
    fbc.xmppconnection.connect().login()
    logger.info("Successfully connected XMPP connection " + fbc.xmppconnection)
    val mucm = MultiUserChatManager getInstanceFor fbc.xmppconnection
    val nick = Resourcepart from "FrankenBot"
    fbc.account.mucs.foreach { m =>
      val muc = mucm getMultiUserChat m
      val mucec = muc.getEnterConfigurationBuilder(nick).requestNoHistory().build()
      muc createOrJoin mucec
      muc addMessageListener (new MessageListener() {
        override def processMessage(m: Message) {
          val b = m getBody
          val fromNick = m.getFrom.getResourceOrNull()

          b match {
            case "Hi Bot" => {
              muc sendMessage ("Hi " + fromNick)
            }
            case "!uptime" => {
              val uptimeMillis = java.lang.management.ManagementFactory.getRuntimeMXBean().getUptime();
              val uptime = DurationFormatUtils.formatDurationWords(uptimeMillis, true, true)
              muc sendMessage (fromNick + ": " + uptime + " uptime")
            }
            case "!check" => {
              maxsMonitor frankenkBotInitializtionComplete
            }
            case _ =>
          }
        }
      })
    }
  }

  // Finally
  maxsMonitor frankenkBotInitializtionComplete

  def initializeConnections(): Seq[FrankenBotConnection] = {
    val yamlConfig = config.yamlConfiguration;
    val accounts = yamlConfig.xmpp.accounts
    val masters = yamlConfig.xmpp.masters

    val connections = new ArrayBuffer[FrankenBotConnection](accounts.size)

    accounts.foreach { a => 
      val confBuilder = XMPPTCPConnectionConfiguration.builder()
        .setUsernameAndPassword(a.jid.getLocalpart, a.pass)
        .setXmppDomain(JidCreate domainBareFrom a.jid.getDomain)

      a.certpin.map {
        pin => confBuilder.setCustomSSLContext(Java7Pinning.forPin(pin))
      }

      val connection = new XMPPTCPConnection(confBuilder build)

      val masterJidsFilter = new OrFilter(masters.map { m => FromMatchesFilter.createBare(m) }:_*)
      connection addAsyncStanzaListener (new StanzaListener() {
        override def processStanza(s: Stanza) {
          val body = s.asInstanceOf[Message].getBody()
          body match {
            case "quit" => {
              logger.info("Received quit from master address '" + s.getFrom + "', calling initiateShutdown");
              FrankenBotMain initiateShutdown
            }
            case _ =>
          }
        }
      }, new AndFilter(MessageWithBodiesFilter.INSTANCE, masterJidsFilter))

      connections += FrankenBotConnection(connection, a)
    }

    connections
  }

  def getDefaultXmppConnection(): XMPPTCPConnection = {
    connections(0).xmppconnection
  }

  def shutdown() {
    connections foreach { c =>
      val mucm = MultiUserChatManager getInstanceFor c.xmppconnection
      c.account.mucs foreach { m =>
        val muc = mucm getMultiUserChat m
        muc leave
      }
      c.xmppconnection disconnect
    }

    maxsMonitor frankenBotShutdown

    val terminateFuture = actorSystem.terminate
    Await.result(terminateFuture, 5 seconds)
  }

  case class FrankenBotConnection(xmppconnection: XMPPTCPConnection, account: XmppAccountConfiguration)
}