/**
 *
 * Copyright 2017 Florian Schmaus
 *
 * This file is part of FrankenBot.
 *
 * FrankenBot is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 */
package eu.geekplace.frankenbot.core

import java.nio.file.Path
import java.nio.file.Paths
import java.nio.file.Files
import java.io.File
import scala.language.postfixOps

object FrankenBotCommandArguments {

  case class CommandArgumentConfiguration(configFile: Option[Path] = None
                                        , dataDirectory: Option[Path] = None
                                        , httpDirectory: Option[Path] = None
                                        )

  def processArgs(args: Array[String]): CommandArgumentConfiguration = {
    val parser = new scopt.OptionParser[CommandArgumentConfiguration]("FB") {
      head("FrankenBot", "TODO INSERT VERSION")
      opt[File]('c', "config") valueName ("<file>") action { (f, c) =>
        c.copy(configFile = Some(f.toPath()))
      }
      opt[File]('d', "datadir") valueName ("<file>") action { (f, c) =>
        c.copy(dataDirectory = Some(f.toPath()))
      }
      opt[File]('h', "httpdir") valueName ("<file>") action { (f, c) =>
        c.copy(httpDirectory = Some(f.toPath()))
      }
    }

    val cac = parser.parse(args, CommandArgumentConfiguration()).get

    if (cac.configFile map { Files.isReadable(_) } filter { _ == true } isEmpty) {
      throw new IllegalStateException()
    }

    cac
  }
}