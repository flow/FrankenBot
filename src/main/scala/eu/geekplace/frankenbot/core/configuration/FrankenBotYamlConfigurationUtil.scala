/**
 *
 * Copyright 2017 Florian Schmaus
 *
 * This file is part of FrankenBot.
 *
 * FrankenBot is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 */
package eu.geekplace.frankenbot.core.configuration

import java.io.File
import java.io.FileInputStream
import java.io.InputStream
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths

import eu.geekplace.frankenbot.core.configuration.FrankenBotYamlConfiguration._
import eu.geekplace.frankenbot.core.configuration.yaml.FrankenBotConfigurationYamlProtocol._

import net.jcazevedo.moultingyaml._

object FrankenBotYamlConfigurationUtil {

  def parseConfig(ymlConf: InputStream): FrankenBotYamlConfiguration = {
    val configString = scala.io.Source.fromInputStream(ymlConf).mkString

    val yaml = configString.parseYaml

    val config = yaml.convertTo[FrankenBotYamlConfiguration]

    config
  }

  def getConfig(yamlConfigurationFile: Path): FrankenBotYamlConfiguration = {

    val fis = new FileInputStream(yamlConfigurationFile.toFile())

    val frankenBotConfig = parseConfig(fis)

    // fis.close()
    frankenBotConfig
  }

}
