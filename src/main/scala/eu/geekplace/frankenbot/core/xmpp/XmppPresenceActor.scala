/**
 *
 * Copyright 2017 Florian Schmaus
 *
 * This file is part of FrankenBot.
 *
 * FrankenBot is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 */
package eu.geekplace.frankenbot.core.xmpp

import org.jxmpp.jid.EntityBareJid
import akka.actor.Actor
import eu.geekplace.frankenbot.core.FrankenBot
import org.jivesoftware.smack.roster.Roster
import org.projectmaxs.xmpp.MaxsStatusExtensionElement
import org.jivesoftware.smack.packet.Presence

object XmppPresenceActor {
  case class GetSingleAvailablePresence(jid: EntityBareJid)
  case class SinglePresence(presence: Presence)
}

class XmppPresenceActor(frankenBot: FrankenBot) extends Actor {
  import XmppPresenceActor._
  val xmppConnection = frankenBot.getDefaultXmppConnection();
  val roster = Roster getInstanceFor xmppConnection

  def receive = {
    case GetSingleAvailablePresence(jid) => {
      val presences = roster getAvailablePresences jid
      if (presences.size() == 1) sender() ! SinglePresence(presences.get(0))
    }
  }
}