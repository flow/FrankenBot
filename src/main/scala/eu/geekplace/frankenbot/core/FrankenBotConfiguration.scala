package eu.geekplace.frankenbot.core

import java.nio.file.Path

import eu.geekplace.frankenbot.core.configuration.FrankenBotYamlConfiguration
import java.nio.file.Files
import java.nio.file.Paths
import eu.geekplace.frankenbot.core.FrankenBotCommandArguments.CommandArgumentConfiguration
import eu.geekplace.frankenbot.core.configuration.FrankenBotYamlConfigurationUtil
import java.io.File
import com.typesafe.scalalogging.Logger
import com.typesafe.scalalogging.LazyLogging

case class FrankenBotConfiguration(configurationFile: Path
                            , dataDirectory: Path
                            , httpDirectory: Path
                            , yamlConfiguration: FrankenBotYamlConfiguration)

object FrankenBotConfiguration extends LazyLogging {

  val defaultConfigLocations = (
    System.getProperty("user.home") + "/Dropbox/development/FrankenBot/FrankenBot.yml"
    :: "FrankenBot.yml"
    :: Nil)

  def fromCommandArgumentConfiguration(cac: CommandArgumentConfiguration): FrankenBotConfiguration = {
    val configurationFile = cac.configFile.getOrElse(lookupExistingDefaultConfigurationFile.get)

    val yamlConfiguration = FrankenBotYamlConfigurationUtil.getConfig(configurationFile);

    val botName = yamlConfiguration.general.name

    lazy val defaultFrankenBotBaseDirectory = "FrankenBots" + File.separatorChar + botName
    var dataDirectory = cac.dataDirectory.getOrElse(Paths.get(defaultFrankenBotBaseDirectory + File.separatorChar + "data"))
    var httpDirectory = cac.httpDirectory.getOrElse(Paths.get(defaultFrankenBotBaseDirectory + File.separatorChar + "http"))
    checkDirectories(dataDirectory, httpDirectory)

    FrankenBotConfiguration(configurationFile, dataDirectory, httpDirectory, yamlConfiguration)
  }

  private def lookupExistingDefaultConfigurationFile = {
    defaultConfigLocations
        .map(Paths get _)
        .filter({p: Path => Files exists p})
        .headOption
  }

  private def checkDirectories(directories: Path*) {
    for (dir <- directories.map(_.toFile)) {
      if (!dir.isDirectory()) {
        logger.info("Creating " + dir)
        dir.mkdirs()
      } else {
        // TODO check if writeable.
      }
    }
  }
}