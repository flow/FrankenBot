/**
 *
 * Copyright 2017 Florian Schmaus
 *
 * This file is part of FrankenBot.
 *
 * FrankenBot is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 */
package org.projectmaxs.xmpp;

import org.jivesoftware.smack.provider.ExtensionElementProvider;
import org.jivesoftware.smack.provider.ProviderManager;
import org.xmlpull.v1.XmlPullParser;

public class MaxsStatusExtensionElementProvider extends ExtensionElementProvider<MaxsStatusExtensionElement> {

    public static final MaxsStatusExtensionElementProvider INSTANCE = new MaxsStatusExtensionElementProvider();

    public static void setup() {
        ProviderManager.addExtensionProvider(MaxsStatusExtensionElement.ELEMENT, MaxsStatusExtensionElement.NAMESPACE, INSTANCE);
    }

    @Override
    public MaxsStatusExtensionElement parse(XmlPullParser parser, int initialDepth) throws Exception {
        int batteryVoltage = -1;
        float batteryPercentage = Float.NaN;
        String powerSource = null;
        String batteryHealth = null;
        float batteryTemperature = Float.NaN;

        outerloop: while (true) {
            int event = parser.next();
            switch (event) {
            case XmlPullParser.START_TAG:
                String name = parser.getName();
                switch (name) {
                case MaxsStatusExtensionElement.BATTERY_HEALTH_ELEMENT:
                    batteryHealth = parser.nextText();
                    break;
                case MaxsStatusExtensionElement.BATTERY_PERCENTAGE_ELEMENT:
                    String batteryPercentageString = parser.nextText();
                    batteryPercentage = Float.parseFloat(batteryPercentageString);
                    break;
                case MaxsStatusExtensionElement.BATTERY_TEMPERATURE_ELEMENT:
                    String batteryTemperatureString = parser.nextText();
                    batteryTemperature = Float.parseFloat(batteryTemperatureString);
                    break;
                case MaxsStatusExtensionElement.BATTERY_VOLTAGE_ELEMENT:
                    String batteryVoltageString = parser.nextText();
                    batteryVoltage = Integer.parseInt(batteryVoltageString);
                    break;
                case MaxsStatusExtensionElement.POWER_SOURCE_ELEMENT:
                    powerSource = parser.nextText();
                    break;
                }
            case XmlPullParser.END_TAG:
                if (parser.getDepth() == initialDepth) {
                    break outerloop;
                }
            }
        }

        return new MaxsStatusExtensionElement(batteryVoltage, batteryPercentage, powerSource, batteryHealth,
                batteryTemperature);
    }

}
