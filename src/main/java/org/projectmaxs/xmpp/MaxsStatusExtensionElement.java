/**
 *
 * Copyright 2017 Florian Schmaus
 *
 * This file is part of FrankenBot.
 *
 * FrankenBot is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 */
package org.projectmaxs.xmpp;

import org.jivesoftware.smack.packet.ExtensionElement;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.util.StringUtils;
import org.jivesoftware.smack.util.XmlStringBuilder;

public class MaxsStatusExtensionElement implements ExtensionElement {

    public static final String ELEMENT = "maxs-status";
    public static final String NAMESPACE = "https://projectmaxs.org";

    public static final String BATTERY_VOLTAGE_ELEMENT = "battery-voltage";
    public static final String BATTERY_PERCENTAGE_ELEMENT = "battery-percentage";
    public static final String POWER_SOURCE_ELEMENT = "power-source";
    public static final String BATTERY_HEALTH_ELEMENT = "battery-health";
    public static final String BATTERY_TEMPERATURE_ELEMENT = "battery-temperature";

    private final int batteryVoltage;
    private final float batteryPercentage;
    private final String powerSource;
    private final String batteryHealth;
    private final float batteryTemperature;

    public MaxsStatusExtensionElement(int batteryVoltage, float batteryPercentage, String powerSource,
            String batteryHealth, float batteryTemperature) {
        this.batteryVoltage = batteryVoltage;
        this.batteryPercentage = batteryPercentage;
        this.powerSource = powerSource;
        this.batteryHealth = batteryHealth;
        this.batteryTemperature = batteryTemperature;
    }

    @Override
    public String getElementName() {
        return ELEMENT;
    }

    @Override
    public String getNamespace() {
        return NAMESPACE;
    }

    public int getBatteryVoltage() {
        return batteryVoltage;
    }

    public float getBatteryPercentage() {
        return batteryPercentage;
    }

    public String getPowerSource() {
        return powerSource;
    }

    public String getBatteryHealth() {
        return batteryHealth;
    }

    public float getBatteryTemperature() {
        return batteryTemperature;
    }

    @Override
    public XmlStringBuilder toXML() {
        XmlStringBuilder xml = new XmlStringBuilder(this);
        xml.rightAngleBracket();

        if (batteryVoltage >= 0) {
            xml.element(BATTERY_VOLTAGE_ELEMENT, Integer.toString(batteryVoltage));
        }
        if (batteryPercentage >= 0) {
            xml.element(BATTERY_PERCENTAGE_ELEMENT, Float.toString(batteryPercentage));
        }
        if (StringUtils.isNotEmpty(powerSource)) {
            xml.element(POWER_SOURCE_ELEMENT, powerSource);
        }
        if (StringUtils.isNullOrEmpty(batteryHealth)) {
            xml.element(BATTERY_HEALTH_ELEMENT, batteryHealth);
        }
        if (batteryTemperature != Float.NaN) {
            xml.element(BATTERY_TEMPERATURE_ELEMENT, Float.toString(batteryTemperature));
        }

        xml.closeElement(this);
        return xml;
    }

    public static MaxsStatusExtensionElement from(Presence presence) {
        return presence.getExtension(ELEMENT, NAMESPACE);
    }
}
